//
//  RVLExchangeViewController.m
//  RVLExchange
//
//  Created by Andrea Murru on 01/08/16.
//  Copyright © 2016 Appiccia. All rights reserved.
//

#import "RVLExchangeViewController.h"
#import "RVLDataProvider.h"
//#import "ContactsViewModelMock.h"
#import "RVLRate.h"
#import "RVLDPAllRates.h"
#import "RVLDPNetworking.h"

@implementation RVLExchangeViewController {
    RVLDPNetworking *manager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
     manager = [[RVLDPNetworking alloc] init];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [manager retrieveAllRates];
}

@end
