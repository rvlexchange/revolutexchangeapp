//
//  main.m
//  RVLExchange
//
//  Created by Andrea Murru on 01/08/16.
//  Copyright © 2016 Appiccia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RVLAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RVLAppDelegate class]));
    }
}
