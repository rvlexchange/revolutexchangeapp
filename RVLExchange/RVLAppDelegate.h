//
//  RVLAppDelegate.h
//  RVLExchange
//
//  Created by Andrea Murru on 01/08/16.
//  Copyright © 2016 Appiccia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RVLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

