//
//  RVLRate.h
//  RVLExchange
//
//  Created by Andrea Murru on 02/08/16.
//  Copyright © 2016 Appiccia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RVLRate : NSObject

@property (nonatomic, strong) NSString *currency;
@property (nonatomic, strong) NSNumber *rate;

- (instancetype) initWithCurrency:(NSString*)currency rate:(NSNumber*)rate;

@end
