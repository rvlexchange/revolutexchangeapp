//
//  RVLDPNetworking.m
//  RVLExchange
//
//  Created by Andrea Murru on 01/08/16.
//  Copyright © 2016 Appiccia. All rights reserved.
//

#import "RVLDPNetworking.h"

#import "RVLDataProvider.h"
#import "RVLDPAbstract.h"
#import "RVLDPAllRates.h"
#import "RVLRate.h"
#import "AFURLSessionManager.h"


@interface RVLDPNetworking ()

@property(nonatomic, strong) NSMutableArray* restActions;
@property(nonatomic, strong) NSPredicate* inProgressPredicate;
@property(nonatomic, strong) NSURLSessionConfiguration *configuration;
@property(nonatomic, strong) AFURLSessionManager *manager;
@property (nonatomic, strong) RVLDPAllRates* provider;
@end

@implementation RVLDPNetworking

- (instancetype) init
{
    self = [super init];
    if (self)
    {
        _configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        _restActions = [NSMutableArray array];
        _inProgressPredicate = [NSPredicate predicateWithFormat:@"inProgress == %@", @(YES)];
        _provider = [[RVLDPAllRates alloc] init];

    }
    return self;
}

- (void) retrieveAllRates
{
    [self executeRestAction:_provider];

    [NSTimer scheduledTimerWithTimeInterval:30.0
                                  target:self
                                selector:@selector(timerFired)
                                userInfo:nil
                                 repeats:YES];


}

- (void)timerFired {
    [self executeRestAction:_provider];
}

- (void)stopRequests {

}


#pragma mark - Common

- (BOOL) executeRestAction:(RVLDPAbstract*)action
{
    // clear completed data providers
    [self.restActions filterUsingPredicate:self.inProgressPredicate];

    if (![self.restActions containsObject:action])
    {
        [self.restActions addObject:action];
        action.inProgress = YES;
        action.client = self.manager;
        [action execute];
        return YES;
    }
    else
    {
        return NO;
    }
}

- (AFURLSessionManager*) manager
{
    if (!_manager)
    {
        _manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:_configuration];
        _manager.securityPolicy.allowInvalidCertificates = YES;
        _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }
    return _manager;
}



@end
