//
//  RVLDPRatesDeserializer.h
//  RVLExchange
//
//  Created by Andrea Murru on 03/08/16.
//  Copyright © 2016 Appiccia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RVLDPRatesDeserializer : NSObject

- (void)extractArrayFromXMLData:(NSData*)data error:(NSError*)error;

@end
