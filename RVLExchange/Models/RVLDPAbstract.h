//
//  RVLDPAbstract.h
//  RVLExchange
//
//  Created by Andrea Murru on 02/08/16.
//  Copyright © 2016 Appiccia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFURLSessionManager.h"

@class AFHTTPRequestOperation;

typedef void(^SuccessBlock)(AFHTTPRequestOperation*, id);

typedef void(^FailureBlock)(AFHTTPRequestOperation*, NSError*);

@interface RVLDPAbstract : NSObject

@property(nonatomic, assign) BOOL inProgress;

@property(nonatomic, strong) AFURLSessionManager* client;

- (void) execute;

- (SuccessBlock) successBlock;

- (FailureBlock) failureBlock;

@end
