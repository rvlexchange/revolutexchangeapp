//
//  RVLConstants.h
//  RVLExchange
//
//  Created by Andrea Murru on 02/08/16.
//  Copyright © 2016 Appiccia. All rights reserved.
//

#import <Foundation/Foundation.h>

NSString* const RVLBaseUrl = @"http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
