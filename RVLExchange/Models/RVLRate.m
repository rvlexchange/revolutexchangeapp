//
//  RVLRate.m
//  RVLExchange
//
//  Created by Andrea Murru on 02/08/16.
//  Copyright © 2016 Appiccia. All rights reserved.
//

#import "RVLRate.h"

@implementation RVLRate

- (instancetype) init {
    if ((self = [super init]))
    {
    }
    return self;
}

- (instancetype)initWithCurrency:(NSString *)currency rate:(NSNumber *)rate {
    self = [super init];

    if ([rate isKindOfClass:[NSString class]]) {
        NSString *currencyString = [NSString stringWithString:currency];
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *rateNumber = [f numberFromString:(NSString*)rate];

        self.currency = currencyString;
        self.rate = rateNumber;
    } else {
        self.currency = currency;
        self.rate = rate;
    }
    return self;
}

@end
