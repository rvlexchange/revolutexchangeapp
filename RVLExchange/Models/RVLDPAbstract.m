//
//  RVLDPAbstract.m
//  RVLExchange
//
//  Created by Andrea Murru on 02/08/16.
//  Copyright © 2016 Appiccia. All rights reserved.
//

#import "RVLDPAbstract.h"
#import "AFHTTPSessionManager.h"
#import "RVLConstants.h"
#import "RVLDPRatesDeserializer.h"

@implementation RVLDPAbstract

- (void) execute
{
    self.inProgress = YES;
    NSURL *URL = [NSURL URLWithString:RVLBaseUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    

    NSURLSessionDataTask *dataTask = [_client dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            NSData * data = (NSData *)responseObject;

            RVLDPRatesDeserializer *deserializer = [[RVLDPRatesDeserializer alloc] init];
            [deserializer extractArrayFromXMLData:data error:error];
        }
    }];
    [dataTask resume];
}


- (SuccessBlock) successBlock
{
    return ^(AFHTTPRequestOperation* operation, id responseObject) {
        self.inProgress = NO;
    };
}

- (FailureBlock) failureBlock
{
    return ^(AFHTTPRequestOperation* operation, NSError* error) {
        self.inProgress = NO;
    };
}

@end
