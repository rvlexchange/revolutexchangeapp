//
//  RVLDPAllRates.m
//  RVLExchange
//
//  Created by Andrea Murru on 01/08/16.
//  Copyright © 2016 Appiccia. All rights reserved.
//

#import "RVLDPAllRates.h"
#import "RVLRate.h"

@implementation RVLDPAllRates

- (SuccessBlock) successBlock
{
    return ^(AFHTTPRequestOperation* operation, id o) {
        NSMutableArray *ratesArray = [[NSMutableArray alloc] init];
        NSArray* rates = o;
        for (NSDictionary* singleRate in rates)
        {
            RVLRate* rate = [[RVLRate alloc] initWithCurrency:singleRate[@"currency"] rate:singleRate[@"rate"]];
            [ratesArray addObject:rate];
            //Call delegate main manager and save list
        }

        _allRates = ratesArray;
        self.inProgress = NO;
    };
}

- (FailureBlock) failureBlock
{
    return ^(AFHTTPRequestOperation* operation, NSError* error) {
    };
}


@end