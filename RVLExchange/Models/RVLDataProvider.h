//
//  RVLDataProvider.h
//  RVLExchange
//
//  Created by Andrea Murru on 02/08/16.
//  Copyright © 2016 Appiccia. All rights reserved.
//

@protocol RVLDataProvider <NSObject>

- (void) retrieveAllRates;

@end