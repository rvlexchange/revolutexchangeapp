//
//  RVLDPRatesDeserializer.m
//  RVLExchange
//
//  Created by Andrea Murru on 03/08/16.
//  Copyright © 2016 Appiccia. All rights reserved.
//

#import "RVLDPRatesDeserializer.h"
#import "XMLReader.h"
#import "RVLDPAllRates.h"

@implementation RVLDPRatesDeserializer

- (void)extractArrayFromXMLData:(NSData*)data error:(NSError*)error {

    NSDictionary *dict = [XMLReader dictionaryForXMLData:data
                                                 options:XMLReaderOptionsProcessNamespaces
                                                   error:&error];


    NSArray *rates = (NSArray*)[dict valueForKeyPath:@"Envelope.Cube.Cube.Cube"];

    RVLDPAllRates *provider = [[RVLDPAllRates alloc] init];

    provider.successBlock(nil, rates);
}

@end
