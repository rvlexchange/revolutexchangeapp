//
//  RVLDPNetworking.h
//  RVLExchange
//
//  Created by Andrea Murru on 01/08/16.
//  Copyright © 2016 Appiccia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RVLDataProvider.h"


@interface RVLDPNetworking : NSObject <RVLDataProvider>

@end