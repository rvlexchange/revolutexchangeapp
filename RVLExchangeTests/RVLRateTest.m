//
//  RVLRateTest.m
//  RVLExchange
//
//  Created by Andrea Murru on 02/08/16.
//  Copyright © 2016 Appiccia. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "RVLRate.h"
#import "RVLDPAllRates.h"



@interface RVLRateTest : XCTestCase
{
    RVLRate* provider;
}
@end

@implementation RVLRateTest

- (void) setUp
{
    [super setUp];
    provider = [[RVLRate alloc] initWithCurrency:@"CAD" rate:@1.4587];
}

- (void) tearDown
{
    provider = nil;
    [super tearDown];
}

- (void) testSuccessBlock
{
    XCTAssertEqualObjects(provider.currency, @"CAD");
    XCTAssertEqualObjects(provider.rate, @1.4587);
}


@end
