//
//  RVLAllRatesTest.m
//  RVLExchange
//
//  Created by Andrea Murru on 02/08/16.
//  Copyright © 2016 Appiccia. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "RVLDPAllRates.h"
#import "RVLRate.h"

@interface RVLAllRatesTest : XCTestCase {
    RVLDPAllRates *provider;
}

@end

@implementation RVLAllRatesTest

- (void) setUp
{
    [super setUp];
    provider = [[RVLDPAllRates alloc] init];
}

- (void) tearDown
{
    [super tearDown];
}

- (void) testSuccessBlock
{
    XCTAssertEqual(provider.allRates.count, 0);

    NSArray* rates = self.mockRates;
    provider.successBlock(nil, rates);

    XCTAssertEqual(provider.allRates.count, 5);
}

- (NSArray*) mockRates
{
    NSDictionary* c1 = @{
                         @"currency" : @"USD",
                         @"rate" : @1.1164
                         };
    NSDictionary* c2 = @{
                         @"currency" : @"JPY",
                         @"rate" : @114.28
                         };
    NSDictionary* c3 = @{
                         @"currency" : @"BGN",
                         @"rate" : @1.9558
                         };
    NSDictionary* c4 = @{
                         @"currency" : @"CZK",
                         @"rate" : @27.029
                         };
    NSDictionary* c5 = @{
                         @"currency" : @"GBP",
                         @"rate" : @0.84540
                         };
    
    return @[ c1, c2, c3, c4, c5 ];
}

@end
