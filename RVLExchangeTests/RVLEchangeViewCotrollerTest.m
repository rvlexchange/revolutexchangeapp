//
//  RVLEchangeViewCotrollerTest.m
//  RVLExchange
//
//  Created by Andrea Murru on 02/08/16.
//  Copyright © 2016 Appiccia. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "RVLExchangeViewController.h"
#import "RVLDataProvider.h"

@interface RVLEchangeViewCotrollerTest : XCTestCase {
    RVLExchangeViewController* viewController;
//    DataProviderMock* dataProvider;
}

@end

@implementation RVLEchangeViewCotrollerTest

- (void) setUp
{
    [super setUp];
//    dataProvider = DataProviderMock.new;
    UIStoryboard* s = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    viewController = [s instantiateViewControllerWithIdentifier:@"RVLExchangeViewController"];
    [viewController loadView];
//    viewController.model = [[ContactsViewModelMock alloc] init];
}

- (void) tearDown
{
    viewController = nil;
    [super tearDown];
}

//- (void) testRelationshipsAfterViewDidLoad
//{
//    RVLExchangeViewController* vc = viewController;
//    XCTAssertNotEqualObjects(vc.tableView.dataSource, vc.model);
//    [vc viewDidLoad];
//    XCTAssertEqualObjects(vc.tableView.dataSource, vc.model);
//}
//
//- (void) testAllRatesAreRetrievedUponViewAppeared
//{
//    XCTAssertFalse(dataProvider.retrieveAllContactsCalled);
//    [viewController viewWillAppear:NO];
//    XCTAssertTrue(dataProvider.retrieveAllContactsCalled);
//}

//- (void) testSelectingContactCellPushesContactDetailsView
//{
//    UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:viewController];
//    XCTAssertEqual(nc.viewControllers.count, 1);
//    [viewController tableView:viewController.tableView
//      didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
//    XCTAssertEqual(nc.viewControllers.count, 2);
//    XCTAssertEqualObjects(nc.topViewController.class, ContactViewController.class);
//    ContactViewController* cvc = (ContactViewController*) nc.topViewController;
//    XCTAssertEqualObjects(cvc.contactID, @"1");
//}

@end
